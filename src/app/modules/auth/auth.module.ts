import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PortalGuard} from "./guards/portal.guard";



@NgModule({
  providers:[PortalGuard]
})
export class AuthModule { }
