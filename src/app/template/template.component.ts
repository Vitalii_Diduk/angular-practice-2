import {Component, ContentChild, ElementRef, Input, OnInit, TemplateRef} from "@angular/core";

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})

export class TemplateComponent {
  @ContentChild(TemplateRef, {static: true})
  template: TemplateRef<ElementRef>;

  @Input()
  elements: string[] =[];

}
