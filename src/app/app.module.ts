import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {TemplateComponent} from "./template/template.component";
import {PortalComponent} from "./portal/portal.component";


@NgModule({
  declarations: [
    AppComponent,
    TemplateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  exports:[
    TemplateComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
