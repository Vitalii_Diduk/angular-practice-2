import {Component, ContentChild, ElementRef, OnInit, TemplateRef} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'practice';

  elements = ['one', 'Vitalii', 'class', 'tab', 'green'];

  fileName(files: File[]){
    alert(files[0].name);
  }
}

