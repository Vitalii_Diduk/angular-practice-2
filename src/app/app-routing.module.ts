import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PortalComponent} from "./portal/portal.component";
import {PortalGuard} from "./modules/auth/guards/portal.guard";

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'portal',
        canActivate: [PortalGuard],
        loadChildren: () => import('./portal/portal.module').then(m => m.PortalModule)
      }
    ]
  },

 // {path: '**', redirectTo: '', pathMatch: 'full'}
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
