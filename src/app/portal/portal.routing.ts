import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PortalComponent} from "./portal.component";
import {TestComponent} from "./test/test.component";

const routes: Routes = [
  {
    path: '',
    component: PortalComponent
  },
  {
    path: 'test',
    loadChildren: () => import('./test/test.module').then(m => m.TestModule)
  },
 // {path: '**', redirectTo: 'portal', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PortalRouting {
}
