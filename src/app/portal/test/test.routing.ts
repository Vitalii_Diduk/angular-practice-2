import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TestComponent} from "./test.component";

const routes: Routes = [
  {
    path: ':id',
    component: TestComponent
  }
 // {path: '**', redirectTo: 'portal', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestRouting {
}
