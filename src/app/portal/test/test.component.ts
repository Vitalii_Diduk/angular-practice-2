import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit{
  title = 'Test';
  constructor(private route: ActivatedRoute) {
  }
  ngOnInit(){
    console.log(this.route.snapshot.params['id']);
  }
}
